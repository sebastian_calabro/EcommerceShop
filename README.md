# EcommerceShop



>Applicacion de un Sistema E Commerce

Sistema realizado en Spring y java 11 en su totalidad.
Se realizo el patron de diseño MVC para manejar el control cliente servidor.
Se realizo un front end sencillo con Angular 9, implementando algunas funcionalidades de la aplicacion en el

# Las tecnologias que utiliza:

- Spring Framework
- Java 11
- Hibernate ORM (One to many Many to one)
- MySQL server
- Scripts SQL
- SpringFox y Swagger 2 para documentar API REST.
- SL4J framework y Logback para manejo de Logging por archivo *logback.xml*.
- JUnit5 y Mockito para testear el service.
- Angular 9, HTML, Bootstrap 5 y CSS para una simple vista del frontend. Esta parte estaria incompleta una parte.
- JPA Repository

# Aplicacion Rest

El link al projecto de postman se encuentra en el siguiente link:


>https://www.postman.com/lunar-module-geologist-54896819/workspace/6a202d85-cf22-4d56-a8c5-5e52d48753ab/collection/13665825-bae44d5d-d320-4326-9186-7b2808f1081f?action=share&creator=13665825

# Request

Crear un carrito 
> POST http://localhost:8080/api/v1/create

Obtiene el status de un carrito
>GET http://localhost:8080/api/v1/cartstatus
Obtiene el status de un carrito
Crear un carrito 
> POST http://localhost:8080/api/v1/create

Este repositorio esta desactualizado, tuve problemas para pullear los cambios del repositorio de github

https://github.com/Calabronx/EcommerceShop aca se encuentra el repo completo

Disculpen las molestias

